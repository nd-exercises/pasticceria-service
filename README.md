# Pasticceria Sevice

Pasticceria Backend API Rest

## Test

È possibile testare le API via [Postman](https://www.postman.com/) utilizzando la collection <[https://www.getpostman.com/collections/ffa510f97285ad022c5e](https://www.getpostman.com/collections/ffa510f97285ad022c5e)>

## Author

- Nico Dante <[info@nicodante.it](mailto:info@nicodante.it)>
