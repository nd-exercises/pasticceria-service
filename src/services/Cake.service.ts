import DbUtil from "../utils/DB.util";
import { Recipe, Ingredient, Cake, CakeShopItem } from "../utils/Common.util";
import * as uuid from "uuid";

class CakeService {
    private dbUtil: DbUtil;

    constructor(dbUtil_?: DbUtil) {
        this.dbUtil = dbUtil_ || DbUtil.getInstance();
    }

    public getRecipeList(filter?: { name?: string, recipeIds?: Array<string> }): Promise<Array<Recipe>> {
        const _self = this;

        return new Promise((resolve: (recipes: Array<Recipe>) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, name, description, base_price FROM recipe WHERE active = true";
            let params: Array<any> = [];

            if (filter) {
                if (filter.name && filter.name.length > 0) {
                    sql += " AND LOWER(name) LIKE ?";
                    params.push("%" + filter.name.toLowerCase() + "%");
                }
                if (filter.recipeIds) {
                    if (filter.recipeIds.length === 0) {
                        return Promise.resolve([]);
                    }

                    sql += " AND id IN (?)";
                    params.push(filter.recipeIds);
                }
            }

            _self.dbUtil.executeQuery(sql, params).then(results => {
                let recipes: Array<Recipe> = [];

                if (results) {
                    results.forEach(resultItem => {
                        recipes.push(_self.fillRecipe(resultItem))
                    });
                }

                return Promise.resolve(recipes);
            }).then(resolve).catch(reject);
        });
    };

    public addRecipe(name: string, basePrice: number, description: string, ingredients: Array<Ingredient>): Promise<Recipe> {
        const _self = this;

        return new Promise((resolve: (recipe: Recipe) => void, reject: (err: Error) => void) => {
            const recipeId: string = uuid.v4();

            _self.dbUtil.executeQuery(`
                INSERT INTO recipe (id, name, description, base_price)
                VALUES (?, ?, ?, ?)
            `, [recipeId, name, description, basePrice]).then(results => {
                let promises: Array<Promise<Ingredient>> = [];

                if (ingredients) {
                    ingredients.forEach(ingredient => {
                        promises.push(_self.addIngredient(recipeId, ingredient.name, ingredient.quantity, ingredient.uom));
                    });
                }

                return Promise.all(promises);
            }).then(createdIngredients => {
                return _self.getRecipe(recipeId);
            }).then(resolve).catch(reject);
        });
    };

    public getRecipe(recipeId: string): Promise<Recipe> {
        const _self = this;

        return new Promise((resolve: (recipe: Recipe) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, name, description, base_price FROM recipe WHERE id = ?";
            let params: Array<any> = [recipeId];

            let recipe: Recipe;

            _self.dbUtil.executeQuery(sql, params).then(results => {
                if (!results || results.length === 0) {
                    return Promise.resolve(null);
                }

                recipe = _self.fillRecipe(results[0]);

                return _self.getIngredientList(recipe.id);
            }).then(ingredients => {
                if (recipe) {
                    recipe.ingredients = ingredients || [];
                }

                return Promise.resolve(recipe);
            }).then(resolve).catch(reject);
        });
    };

    public getIngredientList(recipeId?: string): Promise<Array<Ingredient>> {
        const _self = this;

        return new Promise((resolve: (ingredients: Array<Ingredient>) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, recipe_id, name, quantity, uom FROM ingredient WHERE active = true";
            let params: Array<any> = [];

            if (recipeId && recipeId.length > 0) {
                sql += " AND recipe_id = ?";
                params.push(recipeId);
            }

            _self.dbUtil.executeQuery(sql, params).then(results => {
                let ingredients: Array<Ingredient> = [];

                if (results) {
                    results.forEach(resultItem => {
                        ingredients.push(_self.fillIngredient(resultItem))
                    });
                }

                return Promise.resolve(ingredients);
            }).then(resolve).catch(reject);
        });
    };

    public addIngredient(recipeId: string, name: string, quantity: number, uom: string): Promise<Ingredient> {
        const _self = this;

        return new Promise((resolve: (ingredient: Ingredient) => void, reject: (err: Error) => void) => {
            const ingredientId: string = uuid.v4();

            _self.dbUtil.executeQuery(`
                INSERT INTO ingredient (id, recipe_id, name, quantity, uom)
                VALUES (?, ?, ?, ?, ?)
            `, [ingredientId, recipeId, name, quantity, uom]).then(results => {
                return _self.getIngredient(ingredientId);
            }).then(resolve).catch(reject);
        });
    };

    public getIngredient(recipeId: string): Promise<Ingredient> {
        const _self = this;

        return new Promise((resolve: (recipe: Ingredient) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, recipe_id, name, quantity, uom FROM ingredient WHERE id = ?";
            let params: Array<any> = [recipeId];

            _self.dbUtil.executeQuery(sql, params).then(results => {
                if (!results || results.length === 0) {
                    return Promise.resolve(null);
                }

                return Promise.resolve(_self.fillIngredient(results[0]));
            }).then(resolve).catch(reject);
        });
    };

    public getCakeList(filter?: { name?: string, dateFrom?: Date, dateTo?: Date }): Promise<Array<Cake>> {
        const _self = this;

        return new Promise((resolve: (cakes: Array<Cake>) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, recipe_id, production_date, sale_date FROM cake WHERE active = true";
            let params: Array<any> = [];

            if (filter) {
                if (filter.name && filter.name.length > 0) {
                    sql += " AND LOWER(name) LIKE ?";
                    params.push("%" + filter.name.toLowerCase() + "%");
                }

                if (filter.dateFrom) {
                    sql += " AND production_date >= ?";
                    params.push(filter.dateFrom);
                }

                if (filter.dateTo) {
                    sql += " AND production_date <= ?";
                    params.push(filter.dateTo);
                }
            }

            _self.dbUtil.executeQuery(sql, params).then(results => {
                let cakes: Array<Cake> = [];

                if (results) {
                    results.forEach(resultItem => {
                        cakes.push(_self.fillCake(resultItem))
                    });
                }

                return Promise.resolve(cakes);
            }).then(resolve).catch(reject);
        });
    };

    public getCake(cakeId: string): Promise<Cake> {
        const _self = this;

        return new Promise((resolve: (cake: Cake) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, recipe_id, production_date, sale_date FROM cake WHERE id = ?";
            let params: Array<any> = [cakeId];

            let cake: Cake;

            _self.dbUtil.executeQuery(sql, params).then(results => {
                if (!results || results.length === 0) {
                    return Promise.resolve(null);
                }

                cake = _self.fillCake(results[0]);

                return _self.getRecipe(cake.recipeId);
            }).then(recipe => {
                if (cake) {
                    cake.recipe = recipe;
                }

                return Promise.resolve(cake);
            }).then(resolve).catch(reject);
        });
    };

    public addCake(recipeId: string, productionDate: Date): Promise<Cake> {
        const _self = this;

        return new Promise((resolve: (cake: Cake) => void, reject: (err: Error) => void) => {
            const cakeId: string = uuid.v4();

            _self.dbUtil.executeQuery(`
                INSERT INTO cake (id, recipe_id, production_date)
                VALUES (?, ?, ?)
            `, [cakeId, recipeId, productionDate]).then(results => {
                return _self.getCake(cakeId);
            }).then(resolve).catch(reject);
        });
    };

    public setCakeSaleDate(cakeId: string, saleDate: Date): Promise<Cake> {
        const _self = this;

        return new Promise((resolve: (cake: Cake) => void, reject: (err: Error) => void) => {
            _self.dbUtil.executeQuery(`
                UPDATE cake SET sale_date = ? WHERE id = ?
            `, [saleDate, cakeId]).then(results => {
                return _self.getCake(cakeId);
            }).then(resolve).catch(reject);
        });
    };

    public getCakeShop(filter?: { name?: string, dateFrom?: Date, dateTo?: Date }): Promise<Array<CakeShopItem>> {
        const _self = this;

        return new Promise((resolve: (cakeShopItems: Array<CakeShopItem>) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT recipe_id, recipe_name, base_price, cake_id, production_date, price FROM v_cacke_shop";
            let params: Array<any> = [];

            if (filter) {
                let firstFilter = true;
                if (filter.name && filter.name.length > 0) {
                    sql += " WHERE LOWER(name) LIKE ?";
                    params.push("%" + filter.name.toLowerCase() + "%");

                    firstFilter = false;
                }

                if (filter.dateFrom) {
                    sql += (firstFilter ? " WHERE " : " AND ") + "production_date >= ?";
                    params.push(filter.dateFrom);

                    firstFilter = false;
                }

                if (filter.dateTo) {
                    sql += (firstFilter ? " WHERE " : " AND ") + "production_date <= ?";
                    params.push(filter.dateTo);

                    firstFilter = false;
                }
            }

            _self.dbUtil.executeQuery(sql, params).then(results => {
                let cakeShopItems: Array<CakeShopItem> = [];

                if (results) {
                    results.forEach(resultItem => {
                        cakeShopItems.push(_self.fillCakeShopItem(resultItem))
                    });
                }

                return Promise.resolve(cakeShopItems);
            }).then(resolve).catch(reject);
        });
    };

    private fillRecipe(resultItem: { [columnName: string]: any }): Recipe {
        if (!resultItem) {
            return null;
        }

        let recipe = new Recipe();

        recipe.id = resultItem["id"];
        recipe.createdAt = resultItem["created_at"];
        recipe.active = resultItem["active"];
        recipe.name = resultItem["name"];
        recipe.description = resultItem["description"];
        recipe.basePrice = resultItem["base_price"];

        return recipe;
    };

    private fillIngredient(resultItem: { [columnName: string]: any }): Ingredient {
        if (!resultItem) {
            return null;
        }

        let ingredient = new Ingredient();

        ingredient.id = resultItem["id"];
        ingredient.createdAt = resultItem["created_at"];
        ingredient.active = resultItem["active"];
        ingredient.recipeId = resultItem["recipe_id"];
        ingredient.name = resultItem["name"];
        ingredient.quantity = resultItem["quantity"];
        ingredient.uom = resultItem["uom"];

        return ingredient;
    };

    private fillCake(resultItem: { [columnName: string]: any }): Cake {
        if (!resultItem) {
            return null;
        }

        let cake = new Cake();

        cake.id = resultItem["id"];
        cake.createdAt = resultItem["created_at"];
        cake.active = resultItem["active"];
        cake.recipeId = resultItem["recipe_id"];
        cake.productionDate = resultItem["production_date"];
        cake.saleDate = resultItem["sale_date"];

        return cake;
    };

    private fillCakeShopItem(resultItem: { [columnName: string]: any }): CakeShopItem {
        if (!resultItem) {
            return null;
        }

        let cakeShopItem = new CakeShopItem();

        cakeShopItem.recipeId = resultItem["recipe_id"];
        cakeShopItem.recipeName = resultItem["recipe_name"];
        cakeShopItem.basePrice = resultItem["base_price"];
        cakeShopItem.cakeId = resultItem["cake_id"];
        cakeShopItem.productionDate = resultItem["production_date"];
        cakeShopItem.price = resultItem["price"];

        return cakeShopItem;
    };
};

export default CakeService;