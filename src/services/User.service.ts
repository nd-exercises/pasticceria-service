import DbUtil from "../utils/DB.util";
import { User } from "../utils/Common.util";
import * as uuid from "uuid";

class UserService {
    private dbUtil: DbUtil;

    constructor(dbUtil_?: DbUtil) {
        this.dbUtil = dbUtil_ || DbUtil.getInstance();
    }

    public getLoginUser(email: string, password: string): Promise<User> {
        const _self = this;

        return new Promise((resolve: (user: User) => void, reject: (error: Error) => void) => {
            _self.dbUtil.executeQuery(`
                SELECT 
                    id, created_at, active, email, name, surname, last_login 
                FROM 
                    user 
                WHERE 
                    email = ?
                    AND password = MD5(?)
                    AND active = ?
            `, [email, password, true]).then(results => {
                if (!results || results.length === 0) {
                    return Promise.resolve(null);
                }

                return Promise.resolve(_self.fillUser(results[0]));
            }).then(resolve).catch(reject);
        });
    };

    public getUserList(filter?: { email?: string, name?: string, surname?: string }): Promise<Array<User>> {
        const _self = this;

        return new Promise((resolve: (users: Array<User>) => void, reject: (error: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, email, name, surname, last_login FROM user";
            let params: Array<any> = [];

            if (filter) {
                let firstFilter = true;
                if (filter.email && filter.email.length > 0) {
                    sql += " WHERE LOWER(email) LIKE ?";
                    params.push("%" + filter.email.toLowerCase() + "%");

                    firstFilter = false;
                }

                if (filter.name && filter.name.length > 0) {
                    sql += (firstFilter ? " WHERE " : " AND ") + "LOWER(name) LIKE ?";
                    params.push("%" + filter.name.toLowerCase() + "%");

                    firstFilter = false;
                }

                if (filter.surname && filter.surname.length > 0) {
                    sql += (firstFilter ? " WHERE " : " AND ") + "LOWER(surname) LIKE ?";
                    params.push("%" + filter.surname.toLowerCase() + "%");

                    firstFilter = false;
                }
            }

            _self.dbUtil.executeQuery(sql, params).then(results => {
                let users: Array<User> = [];

                if (results) {
                    results.forEach(resultItem => {
                        users.push(_self.fillUser(resultItem));
                    });
                }

                return Promise.resolve(users);
            }).then(resolve).catch(reject);
        });
    };

    public getUser(userId: string): Promise<User> {
        const _self = this;

        return new Promise((resolve: (user: User) => void, reject: (err: Error) => void) => {
            let sql: string = "SELECT id, created_at, active, email, name, surname, last_login FROM user WHERE id = ?";
            let params: Array<any> = [userId];

            _self.dbUtil.executeQuery(sql, params).then(results => {
                if (!results || results.length === 0) {
                    return Promise.resolve(null);
                }

                return Promise.resolve(_self.fillUser(results[0]));
            }).then(resolve).catch(reject);
        });
    };

    public addUser(email: string, name: string, surname: string, password: string): Promise<User> {
        const _self = this;

        return new Promise((resolve: (user: User) => void, reject: (err: Error) => void) => {
            const userId: string = uuid.v4();

            _self.dbUtil.executeQuery(`
                INSERT INTO user (id, email, name, surname, password, last_login)
                VALUES (?, ?, ?, ?, MD5(?), ?)
            `, [userId, email, name, surname, password, null]).then(results => {
                return _self.getUser(userId);
            }).then(resolve).catch(reject);
        });
    };

    public modifyUser(userId: string, name: string, surname: string, active: boolean, lastLogin:Date, password?: string): Promise<User> {
        const _self = this;

        return new Promise((resolve: (user: User) => void, reject: (err: Error) => void) => {
            let sql: string = "UPDATE user SET name = ?, surname = ?, active = ?, last_login = ?";
            let params: Array<any> = [name, surname, active, lastLogin];

            if (password && password.length > 0) {
                sql += ", password = MD5(?)";
                params.push(password);
            }

            sql += "WHERE id = ?";
            params.push(userId);

            _self.dbUtil.executeQuery(sql, params).then(results => {
                return _self.getUser(userId);
            }).then(resolve).catch(reject);
        });
    };

    private fillUser(resultItem: { [columnName: string]: any }): User {
        if (!resultItem) {
            return null;
        }

        let user = new User();

        user.id = resultItem["id"];
        user.createdAt = resultItem["created_at"];
        user.active = resultItem["active"];
        user.email = resultItem["email"];
        user.name = resultItem["name"];
        user.surname = resultItem["surname"];
        user.lastLogin = resultItem["last_login"];

        return user;
    };
};

export default UserService;
