import { JwtPayload, User } from "../utils/Common.util"
import jwt = require("jsonwebtoken");


class JwtService {
    private jwtSecurityKey: string;
    private jwtExpirationSeconds: number;

    constructor() {
        this.jwtSecurityKey = process.env.JWT_SECURITY_KEY || "my-jwt-key";
        this.jwtExpirationSeconds = Number.parseInt(process.env.JWT_SECURITY_EXPIRATION_SECONDS || "21600");
    };

    public createToken = (user: User, authorizations: Array<string>): Promise<string> => {
        const _self = this;

        let jwtPayload = <JwtPayload>{
            user: user,
            authorizations: authorizations
        };

        let jwtSignOptions = <jwt.SignOptions>{
            expiresIn: _self.jwtExpirationSeconds
        };

        return new Promise((resolve: (token: string) => void, reject: (error: Error) => void): void => {
            jwt.sign(jwtPayload, _self.jwtSecurityKey, jwtSignOptions, (error: Error, jwtToken: string): void => {
                if (error) {
                    reject(error);
                } else {
                    resolve(jwtToken);
                }
            });
        });
    };

    public verifyToken = (token: string): Promise<JwtPayload> => {
        const _self = this;

        return new Promise((resolve: (jwtPayload: JwtPayload) => void, reject: (error: Error) => void): void => {
            jwt.verify(token, _self.jwtSecurityKey, {}, (error: Error, decoded: string | object): void => {
                if (error) {
                    reject(error);
                } else {
                    let jwtPayload = <JwtPayload>(typeof decoded === "string" ? JSON.parse(decoded) : decoded);

                    resolve(jwtPayload);
                }
            });
        });
    };
};

export default JwtService;