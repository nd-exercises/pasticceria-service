import app from "./app";
import AuthController from "./controllers/Auth.controller";
import AdminController from "./controllers/Admin.controller";
import ShopController from "./controllers/Shop.controller";

console.log(" ===== INIT EXPRESS =====");

app.get("/", (req, res) => {
    res.send("Pasticceria APIs");
});

console.log(" ===== INIT ROUTES =====");

try {
    app.use("/api/auth/", new AuthController().getRoutes());
    app.use("/api/admin/", new AdminController().getRoutes());
    app.use("/api/shop/", new ShopController().getRoutes());
} catch (err) {
    console.warn("error on route", err);
}

console.log(" ===== START SERVER =====");

const port: number = Number.parseInt(process.env.PORT || "3000");

app.listen(port, () => {
    console.log(`Pasticceria Services server is started and listening on port ${port}`);
});

const exitHandler = (options: { exit: boolean }, exitCode: number) => {
    if (exitCode || exitCode === 0) {
        console.log(`exit code: ${exitCode}`);
    }
    if (options && options.exit) {
        process.exit();
    }
};

console.log(" ===== INIT PROCESS LISTENERS =====");

//do something when app is closing
process.on("exit", exitHandler.bind(null));

//catches ctrl+c event
process.on("SIGINT", exitHandler.bind(null, { exit: true }));

// catches "kill pid" (for example: nodemon restart)
process.on("SIGUSR1", exitHandler.bind(null, { exit: true }));
process.on("SIGUSR2", exitHandler.bind(null, { exit: true }));

//catches uncaught exceptions
process.on("uncaughtException", exitHandler.bind(null, { exit: true }));
