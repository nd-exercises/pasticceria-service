import { createPool, Pool } from "mysql";

const MIN_MILLIS_TO_LOG = 1000 * 5; // 5 seconds

class DbUtil {
    private static instance: DbUtil;
    private connPool: Pool;

    constructor() {
        this.connPool = createPool({
            host: process.env.MYSQL_HOST || "localhost",
            port: Number.parseInt(process.env.MYSQL_PORT || "3306"),
            database: process.env.MYSQL_DATABASE || "pasticceria",
            user: process.env.MYSQL_USER || "pasticceria",
            password: process.env.MYSQL_PASSWORD || "my-password"
        });
    };

    public static getInstance(): DbUtil {
        if (!DbUtil.instance) {
            DbUtil.instance = new DbUtil();
        }

        return DbUtil.instance;
    };

    public executeQuery(query: string, parameters: Array<any>): Promise<Array<{ [columnName: string]: any }>> {
        const _self = this;

        return new Promise((resolve: (result: Array<{ [columnName: string]: any }>) => void, reject: (error: Error) => void) => {
            const startTime = (new Date()).getTime();

            _self.connPool.query(query, parameters, (err: Error, results: Array<{ [columnName: string]: any }>) => {
                const deltaTime = (new Date()).getTime() - startTime;
                if (deltaTime >= MIN_MILLIS_TO_LOG) {
                    console.warn("slowly query [" + Math.floor(deltaTime / 10) / 100 + " seconds]: " + query + " - " + JSON.stringify(parameters));
                }

                if (err) {
                    reject(err);
                    return;
                }

                resolve(results);
            });
        });
    };
};

export default DbUtil;