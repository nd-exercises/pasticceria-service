import { Router, Request, Response } from "express";

export interface ControllerInterface {
    getRoutes(): Router;
};

export interface AuthRequest extends Request {
    token: string;
};

export class RespError {
    constructor(code: number, message: string) {
        this.code = code;
        this.message = message;
    };

    public code: number;
    public message: string;
};

export const sendError = (res: Response, err: RespError | Error | string) => {
    console.error(err);

    let errorStatus: number = 500;
    let errorMessage: string;
    if (err && (typeof err === "object") && err.message) {
        errorMessage = err.message;

        if (err instanceof RespError && err.code) {
            errorStatus = err.code;
        }
    } else if (err) {
        errorMessage = err.toString();
    } else {
        errorMessage = "unexpected error";
    }

    res.status(errorStatus).json(new RespError(errorStatus, errorMessage));
};

export const getToken = (req: Request): string => {
    let token = (<AuthRequest>req).token;

    if (!token && req.body) {
        token = req.body.token;
    }

    if (!token && req.query) {
        token = req.query.token;
    }

    if (!token && req.params) {
        token = req.params.token;
    }

    return token;
};

export class JwtPayload {
    user: User;
    authorizations: Array<string>;
};

class BaseEntity {
    public id: string;
    public createdAt: Date;
    public active: boolean;
};

export class User extends BaseEntity {
    public email: string;
    public name: string;
    public surname: string;
    public password: string;
    public lastLogin: Date;
};

export class Ingredient extends BaseEntity {
    public recipeId: string;
    public name: string;
    public quantity: number;
    public uom: string;
};

export class Recipe extends BaseEntity {
    public name: string;
    public description: string;
    public basePrice: number;

    public ingredients: Array<Ingredient>;
};

export class Cake extends BaseEntity {
    public recipeId: string;
    public productionDate: Date;
    public saleDate: Date;

    public recipe: Recipe;
};

export class CakeShopItem {
    public recipeId: string;
    public recipeName: string;
    public recipeDescription: string;
    public basePrice: number;
    public cakeId: string;
    public productionDate: Date;
    public price: number;
};
