import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as expressBearerToken from "express-bearer-token";

class App {
    public expressApp: express.Express;

    constructor() {
        this.expressApp = express();

        // parse application/x-www-form-urlencoded
        this.expressApp.use(bodyParser.urlencoded({ extended: true }));

        // parse application/json
        this.expressApp.use(bodyParser.json());

        // enable cors requests
        this.expressApp.use(cors());

        // enable express bearer token parser
        this.expressApp.use(expressBearerToken());
    };
};

export default new App().expressApp;