import { ControllerInterface, sendError, RespError } from "../utils/Common.util";
import { Router, Request, Response } from "express";
import * as moment from "moment";
import CakeService from "../services/Cake.service";

class ShopController implements ControllerInterface {
    private cakeService: CakeService;

    constructor() {
        this.cakeService = new CakeService();
    }

    public cakes(res: Response, filter?: { name?: string, dateFrom?: Date, dateTo?: Date }): void {
        const _self = this;

        _self.cakeService.getCakeShop(filter).then(cakes => {
            res.json(cakes || []);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public cake(res: Response, cakeId: string): void {
        const _self = this;

        _self.cakeService.getCake(cakeId).then(cake => {
            res.json(cake);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public setCakeSaleDate(res: Response, cakeId: string, saleDate: Date | string): void {
        if (!cakeId || cakeId.length === 0
            || !saleDate) {
            sendError(res, new RespError(400, "mandatory fields"));
            return;
        }

        const _self = this;

        _self.cakeService.getCake(cakeId).then(loadedCake => {
            if (!loadedCake) {
                return Promise.reject(new RespError(404, "cake not found"));
            }

            return _self.cakeService.setCakeSaleDate(cakeId, moment(saleDate).toDate());
        }).then(cake => {
            res.json(cake);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public getRoutes(): Router {
        let router = Router();

        router.get("/cake/", (req: Request, res: Response) => {
            new ShopController().cakes(res, {
                name: req.query.name,
                dateFrom: req.query.dateFrom && req.query.dateFrom.length > 0 ? moment(req.query.dateFrom).toDate() : null,
                dateTo: req.query.dateTo && req.query.dateTo.length > 0 ? moment(req.query.dateTo).toDate() : null
            });
        });

        router.get("/cake/:cakeId/", (req: Request, res: Response) => {
            new ShopController().cake(res, req.params.cakeId);
        });

        router.post("/cake/:cakeId/set-sale-date/", (req: Request, res: Response) => {
            new ShopController().setCakeSaleDate(res, req.params.cakeId, req.body.saleDate);
        });

        return router;
    };
};

export default ShopController;