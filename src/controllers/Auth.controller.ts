import { ControllerInterface, sendError, RespError, getToken, User } from "../utils/Common.util";
import { Router, Request, Response } from "express";
import UserService from "../services/User.service";
import JwtService from "../services/Jwt.service";

class AuthController implements ControllerInterface {
    private userService: UserService;
    private jwtService: JwtService;

    constructor() {
        this.userService = new UserService();
        this.jwtService = new JwtService();
    };

    public doLogin(res: Response, email: string, password: string): void {
        const _self = this;

        _self.userService.getLoginUser(email, password).then(user => {
            if (!user) {
                return Promise.reject(new RespError(401, "user not found"));
            }

            return _self.userService.modifyUser(user.id, user.name, user.surname, true, new Date());
        }).then(user => {
            return _self.jwtService.createToken(user, ["main"]);
        }).then(token => {
            res.json({
                token: token
            });
        }).catch(err => {
            sendError(res, err);
        });
    };

    public verify(res: Response, token: string): void {
        const _self = this;

        try {
            _self.jwtService.verifyToken(token).then(jwtPayload => {
                res.json(jwtPayload);
            }).catch(err => {
                sendError(res, new RespError(401, "User not authorized"));
            });
        } catch (err) {
            console.warn("error on validate user authorizations:", err);

            sendError(res, err);
        }
    };

    public getRoutes(): Router {
        let router = Router();

        router.post("/login", (req: Request, res: Response) => {
            new AuthController().doLogin(res, req.body.email, req.body.password);
        });

        router.post("/verify", (req: Request, res: Response) => {
            new AuthController().verify(res, getToken(req));
        });

        return router;
    };
};

export default AuthController;