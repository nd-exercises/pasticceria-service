import { ControllerInterface, AuthRequest, sendError, RespError, User, Recipe, Cake, getToken } from "../utils/Common.util";
import { Router, Request, Response, NextFunction } from "express";
import * as moment from "moment";
import JwtService from "../services/Jwt.service";
import CakeService from "../services/Cake.service";
import UserService from "../services/User.service";

class AdminController implements ControllerInterface {
    private jwtService: JwtService;
    private cakeService: CakeService;
    private userService: UserService;

    constructor() {
        this.jwtService = new JwtService();
        this.cakeService = new CakeService();
        this.userService = new UserService();
    }

    public users(res: Response, filter?: { email?: string, name?: string, surname?: string }): void {
        const _self = this;

        _self.userService.getUserList(filter).then(users => {
            res.json(users || []);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public addUser(res: Response, user: User): void {
        if (!user
            || !user.email || user.email.length === 0
            || !user.name || user.name.length === 0
            || !user.surname || user.surname.length === 0
            || !user.password || user.password.length === 0) {
            sendError(res, new RespError(400, "mandatory fields"));
            return;
        }

        const _self = this;

        _self.userService.addUser(user.email, user.name, user.surname, user.password).then(createdUser => {
            res.json(createdUser);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public user(res: Response, userId: string): void {
        const _self = this;

        _self.userService.getUser(userId).then(user => {
            res.json(user);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public modifyUser(res: Response, userId: string, user: User): void {
        if (!user
            || !user.name || user.name.length === 0
            || !user.surname || user.surname.length === 0) {
            sendError(res, new RespError(400, "mandatory fields"));
            return;
        }

        const _self = this;

        _self.userService.getUser(userId).then(loadedUser => {
            if (!loadedUser) {
                return Promise.reject(new RespError(404, "user not found"));
            }

            return _self.userService.modifyUser(userId, user.name, user.surname, true, loadedUser.lastLogin);
        }).then(createdUser => {
            res.json(createdUser);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public setUserActive(res: Response, userId: string, active: boolean): void {
        const _self = this;

        _self.userService.getUser(userId).then(user => {
            if (!user) {
                return Promise.reject(new RespError(404, "user not found"));
            }

            return _self.userService.modifyUser(userId, user.name, user.surname, active, user.lastLogin);
        }).then(user => {
            res.json(user);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public setUserPassword(res: Response, userId: string, oldPassword: string, newPassword: string): void {
        const _self = this;

        let user: User;
        _self.userService.getUser(userId).then(loadedUser => {
            if (!loadedUser) {
                return Promise.reject(new RespError(404, "user not found"));
            }

            user = loadedUser;

            return _self.userService.getLoginUser(user.email, oldPassword);
        }).then(loadedUser => {
            if (!loadedUser) {
                return Promise.reject(new RespError(401, "password is not correct"));
            }

            return _self.userService.modifyUser(userId, loadedUser.name, loadedUser.surname, loadedUser.active, loadedUser.lastLogin, newPassword);
        }).then(modifiedUser => {
            res.json(modifiedUser);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public recipes(res: Response, filter?: { name?: string }): void {
        const _self = this;

        _self.cakeService.getRecipeList(filter).then(cakes => {
            res.json(cakes || []);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public addRecipe(res: Response, recipe: Recipe): void {
        if (!recipe
            || !recipe.name || recipe.name.length === 0
            || !recipe.basePrice
            || !recipe.ingredients || recipe.ingredients.length === 0) {
            sendError(res, new RespError(400, "mandatory fields"));
            return;
        }

        let ok = true;
        recipe.ingredients.forEach(ingredient => {
            if (!ingredient
                || !ingredient.name || ingredient.name.length === 0
                || !ingredient.quantity
                || !ingredient.uom || ingredient.uom.length === 0) {
                ok = false;
            }
        });

        if (!ok) {
            sendError(res, new RespError(400, "mandatory fields"));
            return;
        }

        const _self = this;

        _self.cakeService.addRecipe(recipe.name, recipe.basePrice, recipe.description, recipe.ingredients).then(createdRecipe => {
            res.json(createdRecipe);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public recipe(res: Response, recipeId: string): void {
        const _self = this;

        _self.cakeService.getRecipe(recipeId).then(recipe => {
            res.json(recipe);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public cakes(res: Response, filter?: { name?: string, dateFrom?: Date, dateTo?: Date }): void {
        const _self = this;

        const recipeIdCakesMap: { [id: string]: Array<Cake> } = {};

        _self.cakeService.getCakeList(filter).then(cakes => {
            let recipeIds: Array<string> = [];

            (cakes || []).forEach(cake => {
                if (!recipeIdCakesMap[cake.recipeId]) {
                    recipeIdCakesMap[cake.recipeId] = [];
                }
                recipeIdCakesMap[cake.recipeId].push(cake);

                recipeIds.push(cake.recipeId);
            });

            return _self.cakeService.getRecipeList({ recipeIds: recipeIds });
        }).then(recipes => {
            let cakes: Array<Cake> = [];

            (recipes || []).forEach(recipe => {
                (recipeIdCakesMap[recipe.id] || []).forEach(cake => {
                    cake.recipe = recipe;

                    cakes.push(cake);
                })
            });

            res.json(cakes);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public addCake(res: Response, recipeId: string, productionDate: Date, quantity: number = 1): void {
        if (!recipeId || recipeId.length === 0
            || !productionDate || quantity < 1) {
            sendError(res, new RespError(400, "mandatory fields"));
            return;
        }

        const _self = this;

        let promises: Array<Promise<Cake>> = [];

        for (let i = 0; i < quantity; i++) {
            promises.push(_self.cakeService.addCake(recipeId, productionDate));
        }

        Promise.all(promises).then(cakes => {
            res.json(cakes);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public cake(res: Response, cakeId: string): void {
        const _self = this;

        _self.cakeService.getCake(cakeId).then(cake => {
            res.json(cake);
        }).catch(err => {
            sendError(res, err);
        });
    };

    public getRoutes(): Router {
        let router = Router();

        const _self = this;

        router.use((req: Request, res: Response, next: NextFunction) => {
            try {
                _self.jwtService.verifyToken(getToken(req)).then(jwtPayload => {
                    res.set("logged-user", JSON.stringify(jwtPayload.user));

                    next();
                }).catch(err => {
                    console.warn("error on validate user authorizations:", err);

                    sendError(res, new RespError(401, "User not authorized"));
                });
            } catch (err) {
                console.warn("error on validate user authorizations:", err);

                sendError(res, err);
            }
        });

        // ========== USERS ==========
        router.get("/user", (req: Request, res: Response) => {
            new AdminController().users(res, {
                email: req.query.email,
                name: req.query.name,
                surname: req.query.surname
            });
        });

        router.put("/user", (req: Request, res: Response) => {
            new AdminController().addUser(res, req.body);
        });

        router.get("/user/:userId/", (req: Request, res: Response) => {
            new AdminController().user(res, req.params.userId);
        });

        router.post("/user/:userId/", (req: Request, res: Response) => {
            new AdminController().modifyUser(res, req.params.userId, req.body);
        });

        router.post("/user/:userId/set-active/", (req: Request, res: Response) => {
            new AdminController().setUserActive(res, req.params.userId, req.body.active);
        });

        router.post("/user/:userId/change-password/", (req: Request, res: Response) => {
            new AdminController().setUserPassword(res, req.params.userId, req.body.oldPassword, req.body.newPassword);
        });

        // ========== SHOP ==========
        router.get("/recipe", (req: Request, res: Response) => {
            new AdminController().recipes(res, {
                name: req.query.name
            });
        });

        router.put("/recipe", (req: Request, res: Response) => {
            new AdminController().addRecipe(res, req.body);
        });

        router.get("/recipe/:recipeId/", (req: Request, res: Response) => {
            new AdminController().recipe(res, req.params.recipeId);
        });

        router.get("/cake", (req: Request, res: Response) => {
            new AdminController().cakes(res, {
                name: req.query.name,
                dateFrom: req.query.dateFrom && req.query.dateFrom.length > 0 ? moment(req.query.dateFrom).toDate() : null,
                dateTo: req.query.dateTo && req.query.dateTo.length > 0 ? moment(req.query.dateTo).toDate() : null
            });
        });

        router.put("/cake", (req: Request, res: Response) => {
            let recipeId: string;
            let productionDate: Date;
            let quantity: number;

            if (req.body) {
                recipeId = req.body.recipeId;
                productionDate = req.body.productionDate;
                quantity = req.body.quantity;
            }

            new AdminController().addCake(res, recipeId, productionDate, quantity);
        });

        router.get("/cake/:cakeId/", (req: Request, res: Response) => {
            new AdminController().cake(res, req.params.cakeId);
        });

        return router;
    };
};

export default AdminController;